from bonus_system import calculateBonuses

STANDARD = "Standard"
PREMIUM = "Premium"
DIAMOND = "Diamond"


def test_invalid_cases():
    assert calculateBonuses("", 100) == 0
    assert calculateBonuses("", 10000) == 0
    assert calculateBonuses("", 10001) == 0
    assert calculateBonuses("", 50000) == 0
    assert calculateBonuses("", 55000) == 0
    assert calculateBonuses("", 100000) == 0
    assert calculateBonuses("", 100001) == 0
    assert calculateBonuses("A", 100) == 0
    assert calculateBonuses("Z", 100) == 0
    assert calculateBonuses("Diam", 100) == 0


def test_standard_program():
    assert calculateBonuses(STANDARD, 100) == 0.5
    assert calculateBonuses(STANDARD, 10000) == 0.5 * 1.5
    assert calculateBonuses(STANDARD, 10001) == 0.5 * 1.5
    assert calculateBonuses(STANDARD, 50000) == 0.5 * 2
    assert calculateBonuses(STANDARD, 55000) == 0.5 * 2
    assert calculateBonuses(STANDARD, 100000) == 0.5 * 2.5
    assert calculateBonuses(STANDARD, 100001) == 0.5 * 2.5


def test_premium_program():
    assert calculateBonuses(PREMIUM, 100) == 0.1
    assert calculateBonuses(PREMIUM, 10000) == 0.1 * 1.5
    assert calculateBonuses(PREMIUM, 10001) == 0.1 * 1.5
    assert calculateBonuses(PREMIUM, 50000) == 0.1 * 2
    assert calculateBonuses(PREMIUM, 55000) == 0.1 * 2
    assert calculateBonuses(PREMIUM, 100000) == 0.1 * 2.5
    assert calculateBonuses(PREMIUM, 100001) == 0.1 * 2.5


def test_diamond_program():
    assert calculateBonuses(DIAMOND, 100) == 0.2
    assert calculateBonuses(DIAMOND, 10000) == 0.2 * 1.5
    assert calculateBonuses(DIAMOND, 10001) == 0.2 * 1.5
    assert calculateBonuses(DIAMOND, 50000) == 0.2 * 2
    assert calculateBonuses(DIAMOND, 55000) == 0.2 * 2
    assert calculateBonuses(DIAMOND, 100000) == 0.2 * 2.5
    assert calculateBonuses(DIAMOND, 100001) == 0.2 * 2.5